#include "SIFT_BBOX_Tracker.h"
#include <opencv2/xfeatures2d/nonfree.hpp>

cv::Point2f centerPoint(std::vector<cv::Point2f> points);
int radius(cv::Point point, std::vector<cv::Point2f>);

SIFT_BBOX_Tracker::SIFT_BBOX_Tracker() :
Tracker(), detector(cv::xfeatures2d::SIFT::create()), matcher(cv::BFMatcher::create()),
targetKeyPoints(), targetDescriptors() {

}

SIFT_BBOX_Tracker::~SIFT_BBOX_Tracker() {

}

// Returns the mean position of a vector of points.
cv::Point2f centerPoint(std::vector<cv::Point2f> points) {
    int i;
    float x_sum = 0;
    float y_sum = 0;
    for (i = 0; i < points.size(); i++) {
        x_sum += points[i].x;
        y_sum += points[i].y;
    }
    
    return cv::Point2f(x_sum/points.size(), y_sum/points.size());
}

// Returns the radius of a circle around a vector of points.
int radius(cv::Point center, std::vector<cv::Point2f> points) {
    int i;
    int x = center.x;
    int y = center.y;
    int radius = 0;
    int dist;
    for (i = 0; i < points.size(); i++) {
        dist = (x-points[i].x)*(x-points[i].x)+(y-points[i].y)*(y-points[i].y);
        dist = sqrt(dist);
        radius = radius < dist ? dist : radius;
    }
    
    return radius;
}

CameraPose SIFT_BBOX_Tracker::initialise(const std::string& targetFile , const cv::Mat& frame, const CameraCalibration& calibration) {
	
	cv::Mat target = cv::imread(targetFile);
	targetImageSize = target.size();
    
	// Detect features in the target image
	detector->detectAndCompute(target, cv::noArray(), targetKeyPoints, targetDescriptors);
	
	// Construct the matching structure
	matcher->add(targetDescriptors);
	matcher->train();
    
    boundingBox.x = 0;
    boundingBox.y = 0;
    boundingBox.height = frame.rows;
    boundingBox.width = frame.cols;

	// Since we are matching not tracking, can just use update now
	return update(frame, calibration);
}


CameraPose SIFT_BBOX_Tracker::update(const cv::Mat& frame, const CameraCalibration& calibration) {

    // Dreate region of interest with bounding box
    cv::Rect roi(boundingBox);
    
    // Draw bounding box
    cv::rectangle(frame, boundingBox, cv::Scalar(255,0,255), 5);
    
	// Detect features in the frame
	std::vector<cv::KeyPoint> frameKeyPoints;
	cv::Mat frameDescriptors;
    detector->detectAndCompute(frame(roi), cv::noArray(), frameKeyPoints, frameDescriptors);
    
	// Match to the target
	std::vector < std::vector<cv::DMatch>> rawMatches;
	matcher->knnMatch(frameDescriptors, rawMatches, 2);
	
	// Filter for ambiguity
	std::vector<cv::Point2f> targetPoints;
	std::vector<cv::Point2f> framePoints;
	for (auto& match : rawMatches) {
		if (match[0].distance < 0.8*match[1].distance) {
			targetPoints.push_back(targetKeyPoints[match[0].trainIdx].pt);
			framePoints.push_back(frameKeyPoints[match[0].queryIdx].pt);
		}
	}
	
	CameraPose cp;
	cp.tracked = false;

	if (targetPoints.size() < 4) {
        // Reset bounding box to whole screen
        boundingBox.x = 0;
        boundingBox.y = 0;
        boundingBox.height = frame.rows;
        boundingBox.width = frame.cols;
		return cp;
	}

	// Find Homography using RANSAC to exclude outliers
	std::vector<int> mask(targetPoints.size(), 0);
	cv::Mat H = cv::findHomography(targetPoints, framePoints, mask, cv::RANSAC);
	
	std::vector<cv::Point3f> objectPoints;
	std::vector<cv::Point2f> imagePoints;
	for (size_t i = 0; i < targetPoints.size(); ++i) {
		if (mask[i]) {
			objectPoints.push_back(cv::Point3f(targetPoints[i].x, targetPoints[i].y, 0));
			imagePoints.push_back(framePoints[i]);
		}
	}
    

	if (objectPoints.size() >= 4) {
		cp.tracked = cv::solvePnP(objectPoints, imagePoints, calibration.K, calibration.d, cp.rvec, cp.tvec);
        
	}
    
    if (cp.tracked) {
        // Find center point of image points
        cv::Point center = centerPoint(imagePoints);
        // calculate radius that encompasses image points
        int r = radius(center, imagePoints);
        
        // Create bounding box
        cv::Rect box;
        box.x = center.x - 2*r;
        box.y = center.y - 2*r;
        box.height = 4*r;
        box.width = 4*r;
        
        // Check the bounding box does not extend outside the screen
        box.x = 0 <= box.x ? box.x : 0;
        box.y = 0 <= box.y ? box.y : 0;
        box.width = 0 <= box.x + box.width ? box.width : frame.cols;
        box.height = 0 <= box.y + box.height ? box.height : frame.rows;
        if (!(box.x + box.width <= frame.cols)) {
            box.x = 0;
            box.width = frame.cols;
        }
        if (!(box.y + box.height <= frame.rows)) {
            box.y = 0;
            box.height = frame.rows;
        }
        boundingBox = box;
    } else {
        // Reset bounding box to whole screen
        boundingBox.x = 0;
        boundingBox.y = 0;
        boundingBox.height = frame.rows;
        boundingBox.width = frame.cols;
    }

	return cp;
}

cv::Size SIFT_BBOX_Tracker::targetSize() {
	return targetImageSize;
}
