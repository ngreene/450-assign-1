#pragma once

#include "CameraCalibration.h"
#include "CameraPose.h"

enum Event {
	NONE = 0,
	SNAP = 1,
	QUIT = 99
};

class Renderer {

public:
	Renderer() = default;
	Renderer(const Renderer&) = delete;
	virtual ~Renderer() = default;

	Renderer& operator=(const Renderer&) = delete;

	virtual Event render(const CameraCalibration& calibration, const CameraPose& pose, const cv::Mat& image) = 0;

};