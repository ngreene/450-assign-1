#pragma once

#include "Tracker.h"

class ChessboardTracker : public Tracker {

public:

	ChessboardTracker();
	~ChessboardTracker();

	CameraPose initialise(const std::string& targetFile, const cv::Mat& frame, const CameraCalibration& calibration);
	CameraPose update(const cv::Mat& frame, const CameraCalibration& calibration);
	cv::Size targetSize();

private:

	std::vector<cv::Point3f> chessboardPoints;
	cv::Size chessboardSize;
};