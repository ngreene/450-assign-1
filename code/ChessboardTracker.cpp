#include "ChessboardTracker.h"

ChessboardTracker::ChessboardTracker() : chessboardSize(0,0) {

}

ChessboardTracker::~ChessboardTracker() {

}

CameraPose ChessboardTracker::initialise(const std::string& targetFile, const cv::Mat& frame, const CameraCalibration& calibration) {
	std::ifstream fin(targetFile);
	fin >> chessboardSize.width >> chessboardSize.height;
	fin.close();

	chessboardPoints.resize(chessboardSize.area());

	size_t ix = 0;
	for (int y = 0; y < chessboardSize.height; ++y) {
		for (int x = 0; x < chessboardSize.width; ++x) {
			chessboardPoints[ix].x = x;
			chessboardPoints[ix].y = y;
			chessboardPoints[ix].z = 0;
			++ix;
		}
	}

	return update(frame, calibration);
}

CameraPose ChessboardTracker::update(const cv::Mat& frame, const CameraCalibration& calibration) {
	
	CameraPose cp;

	std::vector<cv::Point2f> corners;
	cp.tracked = cv::findChessboardCorners(frame, chessboardSize, corners);

	if (cp.tracked) {
		cv::solvePnP(chessboardPoints, corners, calibration.K, calibration.d, cp.rvec, cp.tvec);
	}

	return cp;
}

cv::Size ChessboardTracker::targetSize() {
	return chessboardSize;
}
