#pragma once

#include "Tracker.h"
#include "SIFT_BF_Tracker.h"

class KLT_Tracker : public Tracker {

public:

	KLT_Tracker();
	~KLT_Tracker();

	CameraPose initialise(const std::string& targetFile, const cv::Mat& frame, const CameraCalibration& calibration);
	CameraPose update(const cv::Mat& frame, const CameraCalibration& calibration);
	cv::Size targetSize();

private:
    CameraPose initialise(const cv::Mat& frame, const CameraCalibration& calibration);
	cv::Size targetImageSize;
	std::vector<cv::Point2f> prevPoints;
	std::vector<cv::Point3f> targetPoints;
	cv::Mat prevFrame;
    SIFT_BF_Tracker tempTracker;
};
